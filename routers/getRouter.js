const fs = require('fs');
const express = require('express');
const path = require('path');
const { v4: uuidv4, randomUUID } = require('crypto');
const http = require('http');
const router=express.Router();

const htmlHomePage = path.join(__dirname, '../homePage.html');
const htmlFilePath = path.join(__dirname, '../htmlFile.html');
const jsonFilePath = path.join(__dirname, '../jsonFile.json');
const logFilePath  = path.join(__dirname, '../serverLogFile.log');

router.get('/html', (request, response) => {

    response.sendFile(htmlFilePath);
});

router.get('/json', (request, response) => {

    response.sendFile(jsonFilePath);
});

router.get('/uuid', (request, response) => {

    const uuidV4 = randomUUID();
    response.send({ uuid: uuidV4 });
});

router.get('/status/:status', (request, response) => {
    const status = Number(request.params.status);
    let statusCode = http.STATUS_CODES[status];

    if (statusCode === undefined || status === undefined) {
        response.statusCode = 400;
        response.send({ "Error": "Bad Request: 400", "Enter A Valid Status Code": "!!" });
    }
    else {
        response.statusCode = Number(status);
        response.send({ "status code": `${status}` });
    }

});


router.get('/delay/:time', (request, response) => {
    const { time } = request.params;

    if (time >= 0 && isNaN(time) === false) {
        setTimeout(() => {
            response.send({ "Page Loaded after": `${time} seconds.` });
        }, time * 1000); // defined delay in seconds
    } else {
        response.statusCode = 400;
        response.send({ Error: "Try entering positive numeric values to show delayed output." });
    }
});

router.get('/', (request, response) => {

    fs.readFile(htmlHomePage, 'utf8', (error, data) => {
        if (error) {
            console.error(error);
        } else {
            response.send(data);
        }
    });
});

router.get('/logs', (request,response) => {

    response.sendFile(logFilePath);
})

module.exports=router;