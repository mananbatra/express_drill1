function unexpectedRequest(req,res,next)
{
    next({status:404})
}

module.exports=unexpectedRequest;