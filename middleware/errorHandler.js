const path = require('path');
const errorPagePath= path.join(__dirname,"../errorPage.html")
const pageNotFoundPath= path.join(__dirname,"../pageNotFound.html")

function errorHandler(err,req,res,next)
{
    if(err.status==404)
    {
        res.status(404).sendFile(pageNotFoundPath);
    }else{
        res.status(500).sendFile(errorPagePath);
    }
}

module.exports=errorHandler;